# Real-Time Chat Mobile App - Server and Client 📱💻
This is an example of our expertise in making real time chat systems. The code base includes the server code and client mobile app code. The backend is built using Ratchet framework for WebSockets in PHP. The mobile app is built using the Dart's Flutter framework.

<img src="screenshot.png" height=650px>

## Features 🔥

[✓] **One-to-One Chat**<br>
[✓] **Sent/Delivered/Read Message Receipts**<br>
[✓] **Server Connection Status**<br>
[✓] **Automatic Keep Alive Server Connection**<br>
[✓] **WhatsApp iOS UI**<br>