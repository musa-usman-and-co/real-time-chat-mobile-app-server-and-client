<?php

/// Created by Musa Usman on 1.03.2021
/// Copyright © 2021 Musa Usman & Co. All rights reserved.
///
/// Email: hello@musausman.com
/// Website: musausman.com
/// WhatsApp: +92 324 9066001

namespace ChatServer;

class Receipt
{
    public static function sent($message): array
    {
        return self::receipt(2, $message);
    }

    public static function failed($message): array
    {
        return self::receipt(-1, $message);
    }

    private static function receipt(int $status, array $message): array
    {
        return [
            "message_id" => $message['id'],
            "receiver_id" => $message['receiver_id'],
            "sender_id" => $message['sender_id'],
            "receipt_status" => $status,
            "message_time" => $message['date_time'],
        ];
    }
}
