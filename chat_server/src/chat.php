<?php

/// Created by Musa Usman on 1.03.2021
/// Copyright © 2021 Musa Usman & Co. All rights reserved.
///
/// Email: hello@musausman.com
/// Website: musausman.com
/// WhatsApp: +92 324 9066001

namespace ChatServer;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

use ChatServer\Database;
use ChatServer\Receipt;

class Chat implements MessageComponentInterface
{
    public $clients = [];

    public function onOpen(ConnectionInterface $conn)
    {

        //Get request headers
        $headers = $conn->httpRequest->getHeaders();

        //Get user's ID from headers
        $id = $headers['id'][0];

        // Store the new connection against user's ID
        $this->clients[$id] = $conn;

        echo "New connection! ($id)\n";
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {

        $event = json_decode($msg, true);

        switch ($event['type']) {
            case 'message':
                $this->handleMessageEvent($event);
                break;
            case 'receipt':
                $this->handleReceiptEvent($event);
                break;
            case 'read_all_receipt':
                $this->handleReadAllReceiptEvent($event);
                break;

            default:
                die("\033[01;31mEvent type '{$event['type']}' not handled in onMessage() of chat.php\033[0m");
        }
    }

    public function onClose(ConnectionInterface $conn)
    {

        //Get request headers
        $headers = $conn->httpRequest->getHeaders();

        //Get user's ID from headers
        $id = $headers['id'];

        // The connection is closed, remove it, as we can no longer send it messages
        unset($clients[$id]);

        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "\033[01;31mAn error has occurred: {$e->getMessage()}\n\033[0m";

        $conn->close();
    }

    //Sends the [$event] as an encoded json string to [$receiver].
    public function send($clientID, $event): void
    {
        // Make sure that [$event] matches the Event object structure.
        assert(array_key_exists('type', $event) && array_key_exists('payload', $event));


        if (array_key_exists($clientID, $this->clients)) {
            $this->clients[$clientID]->send(json_encode($event));
        } else {
            echo "Client ($clientID) not connected for message receiving.\n";
        }
    }

    //Wraps the [$payload] inside an Event-structured key-value array.
    //Then forwards that Event to the [send()] function.
    public function sendEvent($clientID, $payload): void
    {
        //Make sure that [$payload] is not an Event object.
        //If so, then the developer should use the [send()] function directly.
        assert(!array_key_exists('type', $payload) && !array_key_exists('payload', $payload));

        //Check if [$payload] matches Message object structure.
        if (array_keys_exists_multi(["id", "receiver_id", "sender_id", "status", "text"], $payload)) {
            $event = [
                'type' => 'message',
                'payload' => $payload,
            ];

            $this->send($clientID, $event);
        } else

            //Check if [$payload] matches Receipt object structure.
            if (array_keys_exists_multi(["message_id", "receipt_status"], $payload)) {
                $event = [
                    'type' => 'receipt',
                    'payload' => $payload,
                ];

                $this->send($clientID, $event);
            } else

                //Check if [$payload] matches ReadAllReceipt object structure.
                if (array_keys_exists_multi(["receiver_id", "sender_id"], $payload)) {
                    $event = [
                        'type' => 'read_all_receipt',
                        'payload' => $payload,
                    ];

                    $this->send($clientID, $event);
                } else {
                    echo ("\033[01;31mChat: SendEvent() function was unable to determine message type for given payload.\nPayload: ");
                    print_r($payload);
                    echo "\n\033[0m";
                    die();
                }
    }

    public function handleMessageEvent($event)
    {

        $receiver = $event['payload']['receiver_id'];
        $sender = $event['payload']['sender_id'];

        //Update message status in the JSON object to 2(Sent to Server)
        $event['payload']['status'] = 2;

        //Save to database
        $insertMessageRespone = Database::insertMessage($event['payload']);

        if ($insertMessageRespone === false) {
            echo "\033[01;31mFailed to save message to database.\n \033[0m";

            //Create a 'MessageFailedReceipt' for the message.
            //This means that the server encountered some sort of error while sending the message.
            $failedReceipt = Receipt::failed($event['payload']);

            //Sent the 'MessageFailedReceipt' to the sender of the message.
            $this->sendEvent($sender, $failedReceipt);
        } else {

            $messageID = $insertMessageRespone;

            //Update message ID in the [$event] object
            $event['payload']['id'] = $messageID;

            //Send message to receiver
            $this->send($receiver, $event);

            //Create a 'MessageSentReceipt' for the message.
            //This means that the server has received the message and saved it to the database.
            $sentReceipt = Receipt::sent($event['payload']);

            //Sent the 'MessageSentReceipt' to the sender of the message.
            //This shows a Single Tick on their message in the mobile app.
            $this->sendEvent($sender, $sentReceipt);
        }
    }

    public function handleReceiptEvent($event)
    {
        $receiver = $event['payload']['receiver_id'];

        //Save updated receipt status to database
        Database::updateMessageStatus($event['payload']);

        $this->send($receiver, $event);
    }

    public function handleReadAllReceiptEvent($event)
    {
        $receiver = $event['payload']['receiver_id'];

        //Save all updated receipt statuses to database
        Database::updateAllMessagesStatus($event['payload']);

        $this->send($receiver, $event);
    }
}
