<?php

/// Created by Musa Usman on 1.03.2021
/// Copyright © 2021 Musa Usman & Co. All rights reserved.
///
/// Email: hello@musausman.com
/// Website: musausman.com
/// WhatsApp: +92 324 9066001

//A function for checking wether an array contains multiple keys.
function array_keys_exists_multi(array $keys, array $arr): bool
{
    return !array_diff_key(array_flip($keys), $arr);
}
