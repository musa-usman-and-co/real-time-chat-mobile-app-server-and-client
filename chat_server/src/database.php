<?php

/// Created by Musa Usman on 1.03.2021
/// Copyright © 2021 Musa Usman & Co. All rights reserved.
///
/// Email: hello@musausman.com
/// Website: musausman.com
/// WhatsApp: +92 324 9066001

namespace ChatServer;

use Exception;
use mysqli;

class Database
{

    public static function connect()
    {

        try {

            $conn = new mysqli(HOST, USERNAME, PASSWORD, DATABASE, PORT);

            if ($conn->connect_errno) {
                die('\033[01;31m Failed to connect to database: ' . $conn->connect_error . "\n \033[0m");
            }

            return $conn;
        } catch (Exception $e) {
            echo "\033[01;31m Failed to connect to database: $e \n \033[0m";
            die();
        }
    }

    public static function insertMessage($message)
    {

        $conn = self::connect();

        $stmt = $conn->prepare(
            "INSERT INTO messages (receiver_id, sender_id, utc_date_time, time_zone, status, text) 
            VALUES (?, ?, ?, ?, ?, ?)"
        );

        $stmt->bind_param("ssssss", $receiverID, $senderID, $utcDateTime, $timeZone, $status, $text);

        $timeInfoSeparated = explode(".", $message['date_time']);

        $utcDateTime = $timeInfoSeparated[0];
        $timeZone = $timeInfoSeparated[1];

        $receiverID = $message["receiver_id"];
        $senderID = $message["sender_id"];
        $status = $message["status"];
        $text = $message["text"];

        if ($stmt->execute()) {
            $messageID = mysqli_insert_id($conn);

            $stmt->close();

            $conn->close();

            return $messageID;
        } else {

            echo "\033[01;31m Failed to save message to database:\n {$stmt->errno}: {$stmt->error}\n \033[0m";

            return false;
        }
    }

    public static function updateMessageStatus($receipt): bool
    {
        print("update message status to db");
        print_r($receipt);

        $conn = self::connect();

        $stmt = $conn->prepare("UPDATE messages SET status = ? WHERE id = ?");

        $stmt->bind_param("ss", $status, $messageID);

        $status = $receipt["receipt_status"];
        $messageID = $receipt["message_id"];

        if ($stmt->execute()) {
            $stmt->close();

            $conn->close();

            return true;
        } else {

            echo "\033[01;31m Failed to save receipt status to database:\n {$stmt->errno}: {$stmt->error}\n \033[0m";

            return false;
        }
    }

    public static function updateAllMessagesStatus($receipt): bool
    {
        print("update message status to db");
        print_r($receipt);

        $conn = self::connect();

        $stmt = $conn->prepare("UPDATE messages SET status = 4 WHERE receiver_id = ?");

        $stmt->bind_param("s", $receiverID);

        $receiverID = $receipt["receiver_id"];

        if ($stmt->execute()) {
            $stmt->close();

            $conn->close();

            return true;
        } else {

            echo "\033[01;31m Failed to save read all receipt status to database:\n {$stmt->errno}: {$stmt->error}\n \033[0m";

            return false;
        }
    }
}
