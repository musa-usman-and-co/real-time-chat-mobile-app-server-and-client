<?php

/// Created by Musa Usman on 1.03.2021
/// Copyright © 2021 Musa Usman & Co. All rights reserved.
///
/// Email: hello@musausman.com
/// Website: musausman.com
/// WhatsApp: +92 324 9066001

require dirname(__DIR__) . '/vendor/autoload.php';

require './src/constants.php';

require './src/chat.php';
require './src/database.php';
require './src/receipt.php';
require './src/utils.php';

use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;
use ChatServer\Chat;
use Ratchet\Http\HttpServer;

$server = IoServer::factory(
    new HttpServer(
        new WsServer(
            new Chat(),
        ),
    ),
    8080
);

echo "\033[01;32mChat server is live!\n\033[01;0m";

$server->run();
