import 'package:chat_app/blocs/connection_bloc.dart';
import 'package:chat_app/blocs/conversations_bloc.dart';
import 'package:chat_app/blocs/messages_bloc.dart';
import 'package:chat_app/pages/login_page.dart';
import 'package:flutter/material.dart';
import 'package:zap_architecture_flutter/zap_architecture_flutter.dart' as ZAP;
import 'package:get/get.dart';

/// Created by Musa Usman on 20.01.2021
/// Copyright © 2021 Musa Usman & Co. All rights reserved.
///
/// Email: hello@musausman.com
/// Website: musausman.com
/// WhatsApp: +1 307-459-1575

void main() {
  Get.put<MessagesBLOC>(MessagesBLOC());
  Get.put<ConversationsBLOC>(ConversationsBLOC());
  Get.put<ConnectionBLOC>(ConnectionBLOC());

  WidgetsFlutterBinding.ensureInitialized();

  ZAP.init();

  runApp(ChatApp());
}

class ChatApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) => MaterialApp(
        theme: ThemeData(
          primaryColor: Color(0xFF017CF6),
          accentColor: Color(0xFF075E54),
          appBarTheme: AppBarTheme(
            iconTheme: IconThemeData(
              color: Color(0xFF017CF6),
            ),
            color: Color(0xFFE2E6EC),
          ),
        ),
        home: LoginPage(),
      );
}
