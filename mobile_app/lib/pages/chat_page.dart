import 'package:chat_app/blocs/connection_bloc.dart';
import 'package:chat_app/blocs/conversations_bloc.dart';
import 'package:chat_app/blocs/messages_bloc.dart';
import 'package:chat_app/utils/globals.dart';
import 'package:chat_app/widgets/message_list_item.dart';
import 'package:chat_app/utils/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:zap_architecture/zap_architecture.dart';

class ChatPage extends StatefulWidget {
  final int otherUserID;
  final String otherUserName;

  const ChatPage(
      {Key key, @required this.otherUserID, @required this.otherUserName})
      : super(key: key);

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  double _messageFieldHeightOld;
  double _messageFieldHeight = 43;

  TextEditingController _messageFieldController = TextEditingController();
  GlobalKey _messageFieldKey = GlobalKey();

  @override
  void initState() {
    _initChat();
    super.initState();
  }

  @override
  Widget build(BuildContext context) => WillPopScope(
        onWillPop: () async {
          Get.find<MessagesBLOC>().clearCurrentChatSession();
          return true;
        },
        child: Scaffold(
          appBar: _appBar(),
          body: _body(),
          backgroundColor: Color(0xFFECE5DD),
        ),
      );

  _appBar() => AppBar(
        centerTitle: false,
        elevation: 10,
        shadowColor: Colors.blueGrey[100].withOpacity(.2),
        iconTheme: IconThemeData(
          color: Theme.of(context).primaryColor,
        ),
        title: Text(
          widget.otherUserName,
          style: TextStyle(
            color: Colors.black,
          ),
        ),
        brightness: Brightness.light,
      );

  _bottomNavigationBar() => Container(
        width: MediaQuery.of(context).size.width - 20,
        margin: EdgeInsets.only(
          left: 5,
          right: 5,
        ),
        padding: EdgeInsets.only(
          bottom: MediaQuery.of(context).padding.bottom + 10,
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(26),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey[600].withOpacity(0.35),
                      blurRadius: 0.5,
                      offset: Offset(0, 0.5),
                    ),
                  ],
                ),
                child: TextField(
                  maxLines: 4,
                  minLines: 1,
                  key: _messageFieldKey,
                  textInputAction: TextInputAction.newline,
                  controller: _messageFieldController,
                  onChanged: _onMessageChanged,
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.white,
                    hintText: "Enter your message...",
                    contentPadding: EdgeInsets.symmetric(
                      vertical: 13.5,
                      horizontal: 20,
                    ),
                    isDense: true,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(26),
                      borderSide: BorderSide.none,
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Container(
              decoration: BoxDecoration(
                color: Theme.of(context).accentColor,
                borderRadius: BorderRadius.circular(26),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey[600].withOpacity(0.35),
                    blurRadius: 0.5,
                    offset: Offset(0, 0.5),
                  ),
                ],
              ),
              child: IconButton(
                splashRadius: 0.1,
                color: Colors.white,
                tooltip: "Send Message",
                icon: Icon(
                  Icons.send_rounded,
                  size: 20,
                ),
                onPressed: _sendMessage,
              ),
            ),
          ],
        ),
      );

  _body() => Stack(
        children: [
          Column(
            children: [
              Expanded(
                child: _bodyContent(),
              ),
              _serverUnavailableBar(),
            ],
          ),
          _messagesStatusBar(),
        ],
      );

  _bodyContent() => Stack(
        children: [
          ///Background
          Container(
            child: Image.network(
              "https://user-images.githubusercontent.com/15075759/28719144-86dc0f70-73b1-11e7-911d-60d70fcded21.png",
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              fit: BoxFit.cover,
            ),
          ),

          ///Foreground
          Column(
            children: [
              ///Messages List View
              Expanded(
                child: GetBuilder<MessagesBLOC>(
                  builder: (MessagesBLOC bloc) => Container(
                    child: ListView.separated(
                      reverse: true,
                      separatorBuilder: (_, __) => SizedBox(height: 5),
                      itemBuilder: (BuildContext context, int index) =>
                          MessageListItem(
                              selfUserID, bloc.currentMessages[index]),
                      itemCount: bloc.currentMessages.length,
                      padding: EdgeInsets.only(
                        left: 20,
                        right: 20,
                        top: 20,
                        bottom: 20 + (_messageFieldHeight ?? 0.0),
                      ),
                    ),
                  ),
                ),
              ),

              ///Message Text Field
              _bottomNavigationBar(),
            ],
          ),
        ],
      );

  _messagesStatusBar() => GetBuilder<MessagesBLOC>(
        builder: (bloc) => AnimatedSwitcher(
          duration: Duration(milliseconds: 500),
          child: AnimatedContainer(
            height: bloc.status.loading
                ? 3
                : bloc.status.error
                    ? 35
                    : 0,
            color: Theme.of(context)
                .errorColor
                .withOpacity(bloc.status.error ? 1 : 0),
            duration: Duration(milliseconds: 500),
            child: bloc.status.loading
                ? LinearProgressIndicator(
                    backgroundColor:
                        Theme.of(context).accentColor.withOpacity(0.25),
                    minHeight: 3,
                  )
                : bloc.status.error
                    ? Center(
                        child: Text(
                          "Failed to fetch messages. Please try again later.",
                          maxLines: 1,
                          textAlign: TextAlign.center,
                          overflow: TextOverflow.ellipsis,
                          style: Theme.of(context)
                              .textTheme
                              .subtitle2
                              .copyWith(color: Colors.white),
                        ),
                      )
                    : null,
          ),
        ),
      );

  _serverUnavailableBar() => GetBuilder<ConnectionBLOC>(
        builder: (ConnectionBLOC bloc) => Container(
          child: AnimatedContainer(
            color: _connectionStatusColor(bloc.connectionStatus),
            width: double.maxFinite,
            duration: Duration(milliseconds: 250),
            height: bloc.connectionStatus.isClear == false
                ? MediaQuery.of(context).padding.bottom + 15 + 15 + 20
                : 0,
            padding: EdgeInsets.only(
              bottom: MediaQuery.of(context).padding.bottom + 15,
              top: 15,
            ),
            child: Center(
              child: Text(
                _connectionStatusText(bloc.connectionStatus),
                style: Theme.of(context)
                    .textTheme
                    .button
                    .copyWith(color: Colors.white),
              ),
            ),
          ),
        ),
      );

  Color _connectionStatusColor(Status connectionStatus) =>
      connectionStatus.loading
          ? Colors.blue
          : connectionStatus.error
              ? Colors.red
              : Colors.green;

  String _connectionStatusText(Status connectionStatus) =>
      connectionStatus.loading
          ? "Connecting..."
          : connectionStatus.error
              ? "Not Connected"
              : "Connected";

  void _onMessageChanged(String _) {
    RenderBox _textFieldBox =
        _messageFieldKey.currentContext.findRenderObject() as RenderBox;
    Size _textFieldSize = _textFieldBox.size;

    if (_messageFieldHeightOld != _textFieldSize.height) {
      setState(() {
        _messageFieldHeight = _textFieldSize.height;
        _messageFieldHeightOld = _textFieldSize.height;
      });
    }
  }

  void _sendMessage() {
    final String _text = _messageFieldController.text.trim();
    if (_text.isNotEmpty) {
      final Message _message = Message(widget.otherUserID, selfUserID, _text);

      _messageFieldController.clear();

      Get.find<MessagesBLOC>().sendMessage(_message);
    }
  }

  Future<void> _initChat() async {
    await Get.find<MessagesBLOC>().initChat(widget.otherUserID);

    //Make sure that unread messages are set to 0
    Get.find<ConversationsBLOC>().markAllMessagesRead(widget.otherUserID);

    //Updating other user that this user has read all their messages.
    Get.find<ConnectionBLOC>().newEvent(
      Event.readAllReceipt(
        ReadAllReceipt(widget.otherUserID, selfUserID),
      ),
    );
  }
}
