import 'package:chat_app/blocs/connection_bloc.dart';
import 'package:chat_app/blocs/conversations_bloc.dart';
import 'package:chat_app/blocs/messages_bloc.dart';
import 'package:chat_app/pages/chat_page.dart';
import 'package:chat_app/pages/login_page.dart';
import 'package:chat_app/utils/globals.dart';
import 'package:chat_app/widgets/conversation_list_item.dart';
import 'package:chat_app/widgets/new_message_dialog.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:web_socket_channel/io.dart';
import 'package:zap_architecture/zap_architecture.dart';

/// Created by Musa Usman on 20.01.2021
/// Copyright © 2021 Musa Usman & Co. All rights reserved.
///
/// Email: hello@musausman.com
/// Website: musausman.com
/// WhatsApp: +1 307-459-1575

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    _initData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: _appBar(context),
        body: _body(context),
        floatingActionButton: _fab(),
      );

  _appBar(BuildContext context) => AppBar(
        centerTitle: true,
        elevation: 10,
        shadowColor: Colors.blueGrey[100].withOpacity(.2),
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "Home #$selfUserID",
              style: TextStyle(
                color: Colors.black,
              ),
            ),
            GetBuilder<ConnectionBLOC>(
              builder: (bloc) => Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    decoration: BoxDecoration(
                      color: _connectionStatusColor(
                          bloc.connectionStatus, bloc.channel),
                      shape: BoxShape.circle,
                    ),
                    height: 7,
                    width: 7,
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    _connectionStatusText(bloc.connectionStatus, bloc.channel),
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 12,
                      letterSpacing: -0.25,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        brightness: Brightness.light,
        actions: [
          IconButton(
            icon: Icon(Icons.logout),
            onPressed: _disconnectGoBack,
          ),
        ],
      );

  Color _connectionStatusColor(
          Status connectionStatus, IOWebSocketChannel channel) =>
      connectionStatus.loading
          ? Colors.blue
          : connectionStatus.error
              ? Colors.red
              : channel != null
                  ? Colors.green
                  : Colors.grey[500];

  String _connectionStatusText(
          Status connectionStatus, IOWebSocketChannel channel) =>
      connectionStatus.loading
          ? "Connecting..."
          : connectionStatus.error
              ? "Not Connected"
              : channel != null
                  ? "Connected"
                  : "Standby";

  _fab() => FloatingActionButton(
        elevation: 0,
        child: Icon(Icons.edit_outlined),
        onPressed: _newChat,
      );

  _body(BuildContext context) => GetBuilder<ConversationsBLOC>(
        builder: (ConversationsBLOC bloc) => ListView.separated(
          separatorBuilder: (_, __) => Divider(
            height: 0.75,
            thickness: 0.75,
            endIndent: 15,
            indent: 15,
          ),
          itemCount: bloc.conversations.values.length,
          itemBuilder: (BuildContext context, int index) =>
              ConversationListItem(bloc.conversations.values.toList()[index]),
        ),
      );

  void _disconnectGoBack() {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (BuildContext context) => LoginPage(),
      ),
    );

    Get.find<ConnectionBLOC>().disconnect();
    Get.find<ConversationsBLOC>().clearAll();
    Get.find<MessagesBLOC>().clearAll();
  }

  void _newChat() async {
    int otherID = await showDialog<int>(
      context: context,
      builder: (_) => NewMessageDialog(),
    );

    if (otherID != null) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (BuildContext context) => ChatPage(
            otherUserID: otherID,
            otherUserName: 'User #$otherID',
          ),
        ),
      );
    }
  }

  Future<void> _initData() async {
    await Get.find<ConnectionBLOC>().connect(selfUserID);

    Future.delayed(
      Duration(milliseconds: 150),
      () => Get.find<ConversationsBLOC>().loadInitialData(),
    );
  }
}
