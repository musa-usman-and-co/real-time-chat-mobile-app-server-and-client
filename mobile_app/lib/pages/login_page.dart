import 'package:chat_app/pages/home_page.dart';
import 'package:chat_app/utils/globals.dart';
import 'package:flutter/material.dart';

/// Created by Musa Usman on 20.01.2021
/// Copyright © 2021 Musa Usman & Co. All rights reserved.
///
/// Email: hello@musausman.com
/// Website: musausman.com
/// WhatsApp: +1 307-459-1575

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController _idController = TextEditingController(text: "1");
  final TextEditingController _nameController =
      TextEditingController(text: "Musa Usman");

  @override
  Widget build(BuildContext context) => Scaffold(
        body: _body(),
      );

  _body() => Container(
        padding: EdgeInsets.symmetric(
          horizontal: 15,
        ),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Chat App",
                style: Theme.of(context).textTheme.headline6.copyWith(
                      fontWeight: FontWeight.bold,
                    ),
              ),
              SizedBox(
                height: 30,
              ),
              TextFormField(
                controller: _idController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  hintText: "1234",
                  labelText: "Your User ID",
                  icon: Icon(Icons.emoji_emotions_outlined),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              TextFormField(
                controller: _nameController,
                decoration: InputDecoration(
                  hintText: "Musa Usman",
                  labelText: "Your Name",
                  icon: Icon(Icons.person),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              RaisedButton(
                elevation: 0,
                color: Colors.blue,
                child: Text(
                  "Continue",
                  style: Theme.of(context).textTheme.button.copyWith(
                        color: Colors.white,
                      ),
                ),
                onPressed: _continue,
              ),
            ],
          ),
        ),
      );

  void _continue() {
    int userID = int.parse(_idController.text);

    selfUserID = userID;

    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (BuildContext context) => HomePage(),
      ),
    );
  }
}
