import 'package:chat_app/utils/globals.dart';
import 'package:chat_app/utils/models.dart';
import 'package:zap_architecture/zap_architecture.dart';
import 'package:zap_architecture_flutter/zap_architecture_flutter.dart';

/// Created by Musa Usman on 24.01.2021
/// Copyright © 2021 Musa Usman & Co. All rights reserved.
///
/// Email: hello@musausman.com
/// Website: musausman.com
/// WhatsApp: +1 307-459-1575

API api = API("http://10.10.51.236:5600");

class API extends HTTPMixin {
  API(String serverURL) : super(serverURL);

  Future<Response<List<Conversation>>> conversations() async {
    try {
      Response<List<dynamic>> response = await request<List<dynamic>>(
        "/conversations",
        headers: {"id": selfUserID},
      );

      List<Conversation> conversations = [];

      if (response.success) {
        response.data.forEach((element) {
          conversations.add(Conversation.fromJSON(element));
        });
        return Response.success(conversations);
      } else
        return Response.error();
    } catch (e, s) {
      print("API: Conversations: UnexpectedError: $e\n$s");
      return Response.error();
    }
  }

  Future<Response<List<Message>>> messages(int otherID) async {
    try {
      Response<List<dynamic>> response = await request<List<dynamic>>(
        "/messages",
        headers: {"id": selfUserID, "other_user_id": otherID},
      );

      List<Message> messages = [];

      if (response.success) {
        response.data.forEach((element) {
          messages.add(Message.fromJSON(element));
        });
        return Response.success(messages);
      } else
        return Response.error();
    } catch (e, s) {
      print("API: Messages: UnexpectedError: $e\n$s");
      return Response.error();
    }
  }
}
