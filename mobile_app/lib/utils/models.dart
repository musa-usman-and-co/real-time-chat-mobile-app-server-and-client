import 'package:chat_app/utils/globals.dart';

/// Created by Musa Usman on 20.01.2021
/// Copyright © 2021 Musa Usman & Co. All rights reserved.
///
/// Email: hello@musausman.com
/// Website: musausman.com
/// WhatsApp: +1 307-459-1575

class Event {
  final String type;
  final EventPayload payload;

  Event.message(Message message)
      : this.type = 'message',
        this.payload = message;

  Event.receipt(Receipt receipt)
      : this.type = 'receipt',
        this.payload = receipt;

  Event.readAllReceipt(ReadAllReceipt readAllReceipt)
      : this.type = 'read_all_receipt',
        this.payload = readAllReceipt;

  Event.fromJSON(Map json)
      : this.type = json['type'],
        this.payload = json['type'] == 'message'
            ? Message.fromJSON(json['payload'])
            : json['type'] == 'receipt'
                ? Receipt.fromJSON(json['payload'])
                : ReadAllReceipt.fromJSON(json['payload']);

  Map<String, dynamic> toJSON() => {
        "type": this.type,
        "payload": this.payload.toJSON(),
      };
}

abstract class EventPayload {
  Map<String, dynamic> toJSON();
}

class Conversation {
  final String otherUserName;
  final int otherUserID;

  int unreadMessageCount;
  Message lastMessage;

  Conversation(this.otherUserID, this.lastMessage)
      : this.unreadMessageCount = 0,
        this.otherUserName = "User #$otherUserID";

  Conversation.fromJSON(Map json)
      : this.otherUserID = json['user_id'],
        this.otherUserName = json['user_name'],
        this.unreadMessageCount = json['unread_message_count'] ?? 0,
        this.lastMessage = Message.fromJSON(json["last_message"]);
}

class Message extends EventPayload {
  int id;
  int status;
  final int receiverID;
  final int senderID;
  final String text;
  final DateTime dateTime;

  String dateTimeString;

  int get otherUserID =>
      this.senderID == selfUserID ? this.receiverID : this.senderID;

  Message(this.receiverID, this.senderID, this.text)
      : this.status = 1,
        this.dateTime = DateTime.now(),
        this.id = null {
    this.dateTimeString = _timeString(dateTime);
  }

  Message.fromJSON(Map json)
      : this.id = json['id'],
        this.receiverID = json['receiver_id'],
        this.senderID = json['sender_id'],
        this.text = json['text'],
        this.status = json['status'],
        this.dateTime = DateTime.parse(json['date_time']).toLocal() {
    this.dateTimeString = _timeString(dateTime);
  }

  Map<String, dynamic> toJSON() => {
        "id": this.id,
        "receiver_id": this.receiverID,
        "sender_id": this.senderID,
        "text": this.text,
        'status': this.status,
        'date_time': this.dateTime.toUtc().toString(),
      };

  @override
  String toString() => this.text;

  void updateReceipt(Receipt receipt) {
    //Status #2 means Sent To Server.
    //This means that the server has successfully saved the message in the database.
    //We use the message ID from the receipt data sent by the server to update the ID locally.
    if (receipt.receiptStatus == 2) this.id = receipt.messageID;

    if (receipt.receiptStatus > this.status)
      this.status = receipt.receiptStatus;
  }
}

class Receipt extends EventPayload {
  /// Used to identify and assign an ID to a message object when it is saved
  /// and returned from the database.
  /// This is only used during a SENT receipt.
  final DateTime messageTime;

  ///Receipt Variables
  final int messageID;
  final int senderID;
  final int receiverID;
  final int receiptStatus;

  int get otherUserID =>
      this.senderID == selfUserID ? this.receiverID : this.senderID;

  static Receipt sent(Message message) => Receipt._internal(2, message);

  static Receipt delivered(Message message) => Receipt._internal(3, message);

  static Receipt read(Message message) => Receipt._internal(4, message);

  Receipt._internal(this.receiptStatus, Message message)
      : this.messageID = message?.id,
        this.senderID = selfUserID,
        this.receiverID = message?.otherUserID,
        this.messageTime = message?.dateTime;

  @override
  Map<String, dynamic> toJSON() => {
        "message_id": this.messageID,
        "receiver_id": this.receiverID,
        "sender_id": this.senderID,
        "receipt_status": this.receiptStatus,
      };

  Receipt.fromJSON(Map json)
      : this.receiverID = json['receiver_id'],
        this.senderID = json['sender_id'],
        this.messageID = json['message_id'],
        this.receiptStatus = json['receipt_status'],
        this.messageTime = json['message_time'] != null
            ? DateTime.parse(json['message_time']).toLocal()
            : null;
}

class ReadAllReceipt extends EventPayload {
  final int receiverID;
  final int senderID;

  ReadAllReceipt(this.receiverID, this.senderID);

  @override
  Map<String, dynamic> toJSON() =>
      {'receiver_id': receiverID, 'sender_id': senderID};

  ReadAllReceipt.fromJSON(Map json)
      : this.receiverID = json['receiver_id'],
        this.senderID = json['sender_id'];
}

String _timeString(DateTime dateTime) =>
    "${dateTime.hour > 12 ? dateTime.hour - 12 : dateTime.hour}:${dateTime.minute < 10 ? "0" + dateTime.minute.toString() : dateTime.minute} ${dateTime.hour > 11 ? "pm" : "am"}";
