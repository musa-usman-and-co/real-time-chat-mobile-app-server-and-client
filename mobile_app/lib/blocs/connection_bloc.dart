import 'dart:async';
import 'dart:convert';
import 'package:chat_app/blocs/messages_bloc.dart';
import 'package:chat_app/utils/globals.dart';
import 'package:chat_app/utils/models.dart';
import 'package:get/get.dart';
import 'package:web_socket_channel/io.dart';
import 'package:zap_architecture/zap_architecture.dart';

/// Created by Musa Usman on 20.01.2021
/// Copyright © 2021 Musa Usman & Co. All rights reserved.
///
/// Email: hello@musausman.com
/// Website: musausman.com
/// WhatsApp: +1 307-459-1575

class ConnectionBLOC extends GetxController {
  IOWebSocketChannel channel;

  Timer _reEstablishConnectionTimer;

  Status _connectionStatus = Status.clear();

  Status get connectionStatus => _connectionStatus;

  Status _oldConnectionStatus = Status.clear();

  StreamSubscription eventsSubscription;

  set connectionStatus(Status connectionStatus) {
    if (connectionStatus.loading != _oldConnectionStatus.loading ||
        connectionStatus.error != _oldConnectionStatus.error) {
      _connectionStatus = connectionStatus;
      _oldConnectionStatus = connectionStatus;

      update();
    }
  }

  Future<void> connect(int userID) async {
    try {
      connectionStatus = Status.loading();

      await Future.delayed(Duration(seconds: 1));

      channel = IOWebSocketChannel.connect(
        'ws://192.168.1.91:8080/socket',
        headers: {'id': userID},
      );

      connectionStatus = Status.clear();

      eventsSubscription = channel.stream.listen(_handleEvent);

      eventsSubscription.onDone(() => _reEstablishConnection(userID));

      update();
    } catch (e, s) {
      print("ConnectionBLOC: Connect: $e\n$s");
      connectionStatus = Status.error();
    }
  }

  void disconnect() {
    channel.sink.close();
    connectionStatus = Status.clear();
    eventsSubscription?.cancel();
  }

  /// This is the core function for sending data to the server.
  void newEvent(Event event) {
    print(event.toJSON());
    channel.sink.add(jsonEncode(event.toJSON()));
  }

  /// Endlessly keeps on trying to connect to the chat server, every 3 seconds.
  _reEstablishConnection(int userID) {
    connectionStatus = Status.error();

    _reEstablishConnectionTimer = Timer.periodic(
      Duration(seconds: 3),
      (_) => _attemptReconnect(userID),
    );
  }

  _handleEvent(dynamic rawEvent) {
    Event event = _parseEvent(rawEvent);

    if (event.payload is Message) {
      Get.find<MessagesBLOC>().handleNewMessage(event.payload);

      print(
          "Sending delivered receipt: $selfUserID -> ${(event.payload as Message).otherUserID}: ${(event.payload as Message).id}");

      //Dispatch a delivered receipt
      newEvent(
        Event.receipt(
          Receipt.delivered(event.payload),
        ),
      );
    } else if (event.payload is Receipt) {
      print(
          "Received receipt $selfUserID <- ${(event.payload as Receipt).otherUserID}: ${(event.payload as Receipt).messageID}: ${(event.payload as Receipt).receiptStatus}: DateTime(${(event.payload as Receipt).messageTime})");
      Get.find<MessagesBLOC>().handleMessageReceiptStatus(event.payload);
    } else if (event.payload is ReadAllReceipt) {
      print(
          "Received read all receipt $selfUserID <- ${(event.payload as ReadAllReceipt).receiverID}");
      Get.find<MessagesBLOC>().handleMessagesReadAllReceipt(event.payload);
    }
  }

  _parseEvent(dynamic rawEvent) {
    if (rawEvent is Event)
      return rawEvent;
    else {
      Map<String, dynamic> _eventMap = jsonDecode(rawEvent);
      return Event.fromJSON(_eventMap);
    }
  }

  _attemptReconnect(int userID) async {
    print("RECONNECTING AS $userID");

    await connect(userID);

    if (connectionStatus.isClear) _reEstablishConnectionTimer?.cancel();
  }
}
