import 'package:chat_app/blocs/connection_bloc.dart';
import 'package:chat_app/blocs/conversations_bloc.dart';
import 'package:chat_app/utils/api_interface.dart';
import 'package:chat_app/utils/models.dart';
import 'package:get/get.dart' hide Response;
import 'package:zap_architecture/zap_architecture.dart';

/// Created by Musa Usman on 21.01.2021
/// Copyright © 2021 Musa Usman & Co. All rights reserved.
///
/// Email: hello@musausman.com
/// Website: musausman.com
/// WhatsApp: +1 307-459-1575

class MessagesBLOC extends GetxController with StatusMixin {
  /// User ID of the person, that the the client is chatting with on the [ChatPage].
  int _currentChatOtherUserID;

  /// List of messages for the current chat.
  List<Message> get currentMessages => _messages[_currentChatOtherUserID];

  /// All messages in memory against user IDs
  Map<int, List<Message>> _messages = {};

  Future<void> initChat(int otherUserID) async {
    _currentChatOtherUserID = otherUserID;
    _messages.putIfAbsent(otherUserID, () => []);

    status = Status.loading();

    Response<List<Message>> response = await api.messages(otherUserID);

    if (response.success) _messages[otherUserID] = response.data;

    status = Status.fromResponse(response);
  }

  /// Sends the message to the server via [ConnectionBLOC].
  /// Inserts the message into the app locally via [handleNewMessage].
  void sendMessage(Message message) {
    Get.find<ConnectionBLOC>().newEvent(Event.message(message));
    handleNewMessage(message);
  }

  void handleNewMessage(Message message) {
    int otherUserID = message.otherUserID;

    _messages.putIfAbsent(otherUserID, () => []);
    _messages[otherUserID].insert(0, message);

    print("CURRENT CHAT OTHER USER ID: $_currentChatOtherUserID");

    //Updates UI only if the user is on the ChatPage with the other person.
    if (otherUserID == _currentChatOtherUserID) update();

    print("$_messages");

    //Increment Unread Message count if not chatting with them right now.
    bool incrementUnreadMessages = otherUserID != _currentChatOtherUserID;

    //Update conversations data
    Get.find<ConversationsBLOC>()
        .handleNewMessage(otherUserID, message, incrementUnreadMessages);

    if (otherUserID == _currentChatOtherUserID)
      Get.find<ConnectionBLOC>().newEvent(
        Event.receipt(
          Receipt.read(message),
        ),
      );
  }

  void handleMessageReceiptStatus(Receipt receipt) {
    int otherUserID = receipt.otherUserID;

    if (_messages.containsKey(otherUserID)) {
      //Special method for using DateTime to find the exact message and update
      //it's ID and status.
      if (receipt.receiptStatus == 2) {
        _messages[otherUserID]
            .reversed
            .firstWhere((element) =>
                element.dateTime.isAtSameMomentAs(receipt.messageTime))
            .updateReceipt(receipt);
      } else {
        _messages[otherUserID]
            .where((Message message) => message.id == receipt.messageID)
            .forEach((element) => element.updateReceipt(receipt));
      }

      update();

      Get.find<ConversationsBLOC>().handleLastMessageReceipt(receipt);
    }
  }

  void handleMessagesReadAllReceipt(ReadAllReceipt readAllReceipt) {
    int otherUserID = readAllReceipt.senderID;

    if (_messages.containsKey(otherUserID)) {
      _messages[otherUserID].forEach((element) {
        if (element.status != 1) element.updateReceipt(Receipt.read(null));
      });
    }

    //Updates UI only if the user is on the ChatPage with the other person.
    if (otherUserID == _currentChatOtherUserID) update();

    Get.find<ConversationsBLOC>().handleMessagesReadAllReceipt(otherUserID);
  }

  void clearAll() {
    _messages = {};
    _currentChatOtherUserID = null;
  }

  void clearCurrentChatSession() {
    _currentChatOtherUserID = null;
  }
}
