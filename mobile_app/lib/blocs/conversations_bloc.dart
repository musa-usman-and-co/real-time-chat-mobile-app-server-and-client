import 'package:chat_app/utils/api_interface.dart';
import 'package:chat_app/utils/globals.dart';
import 'package:chat_app/utils/models.dart';
import 'package:get/get.dart' hide Response;
import 'package:zap_architecture/zap_architecture.dart';

/// Created by Musa Usman on 21.01.2021
/// Copyright © 2021 Musa Usman & Co. All rights reserved.
///
/// Email: hello@musausman.com
/// Website: musausman.com
/// WhatsApp: +1 307-459-1575

class ConversationsBLOC extends GetxController with StatusMixin {
  /// List of conversations against each user's id
  Map<int, Conversation> _conversations = {};

  Map<int, Conversation> get conversations => _conversations;

  set conversations(Map<int, Conversation> conversations) {
    _conversations = conversations;
    update();
  }

  Future<void> loadInitialData() async {
    status = Status.loading();

    Response<List<Conversation>> response = await api.conversations();

    if (response.success)
      response.data.forEach((Conversation conversation) {
        _conversations[conversation.otherUserID] = conversation;
      });

      status = Status.fromResponse(response);
  }

  /// Makes sure the conversations data is updated according to the new message.
  void handleNewMessage(
      int otherUserID, Message message, bool incrementUnreadMessages) async {
    //Makes sure that the conversation has been added
    _conversations.putIfAbsent(
      otherUserID,
      () => Conversation(otherUserID, message),
    );

    conversations[otherUserID].lastMessage = message;

    if (message.receiverID == selfUserID && incrementUnreadMessages)
      conversations[otherUserID].unreadMessageCount += 1;

    update();
  }

  void handleLastMessageReceipt(Receipt receipt) {
    int otherUserID = receipt.otherUserID;
    if (conversations.containsKey(otherUserID)) {
      if (conversations[otherUserID].lastMessage.id == receipt.messageID)
        conversations[otherUserID].lastMessage.updateReceipt(receipt);

      update();
    }
  }

  void handleMessagesReadAllReceipt(int otherUserID) {
    print("handleMessagesReadAllReceipt: $otherUserID");
    if (conversations.containsKey(otherUserID)) {
      conversations[otherUserID].lastMessage.updateReceipt(Receipt.read(null));
      update();
    }
  }

  void markAllMessagesRead(int otherUserID) {
    conversations[otherUserID]?.unreadMessageCount = 0;
    update();
  }

  void clearAll() {
    conversations = {};
  }
}

mixin StatusMixin on GetxController {
  Status _status = Status.clear();

  Status get status => _status;

  set status(Status taskStatus) {
    _status = taskStatus;
    update();
  }
}
