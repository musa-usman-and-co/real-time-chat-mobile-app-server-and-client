import 'package:chat_app/utils/models.dart';
import 'package:chat_app/widgets/receipt_check_icon.dart';
import 'package:flutter/material.dart';

/// Created by Musa Usman on 20.01.2021
/// Copyright © 2021 Musa Usman & Co. All rights reserved.
///
/// Email: hello@musausman.com
/// Website: musausman.com
/// WhatsApp: +1 307-459-1575

class MessageListItem extends StatelessWidget {
  final Message message;
  final int selfID;

  const MessageListItem(this.selfID, this.message);

  @override
  Widget build(BuildContext context) {
    bool fromSelfUser = message.senderID == selfID;

    return _ListItemUI(
      text: message.text,
      status: message.status,
      dateTime: message.dateTimeString,
      fromSelfUser: fromSelfUser,
    );
  }
}

class _ListItemUI extends StatelessWidget {
  final int status;
  final String text;
  final String dateTime;
  final bool fromSelfUser;

  const _ListItemUI(
      {Key key,
      @required this.text,
      @required this.status,
      @required this.dateTime,
      @required this.fromSelfUser})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double _horizontalPadding = 8;
    final double _verticalPadding = 6;
    final TextStyle _messageTextStyle = TextStyle(
      fontSize: 14,
      fontWeight: FontWeight.w500,
    );

    final TextStyle _timeTextStyle = TextStyle(
      fontSize: 10,
      fontWeight: FontWeight.w500,
      color: Colors.grey[600],
    );

    final Size _messageTextSize = _layoutTextSize(text, _messageTextStyle);
    final Size _timeTextSize = _layoutTextSize(dateTime, _timeTextStyle);

    final double _listTileWidth = _messageTextSize.width +
        _timeTextSize.width +
        (_horizontalPadding * 2) +
        (fromSelfUser ? 26 : 5);

    final double _radius = 7;

    BorderRadius _borderRadius = BorderRadius.only(
      topLeft: Radius.circular(_radius),
      topRight: Radius.circular(_radius),
      bottomRight: Radius.circular(fromSelfUser ? 0 : _radius),
      bottomLeft: Radius.circular(!fromSelfUser ? 0 : _radius),
    );

    return Align(
      alignment: fromSelfUser ? Alignment.centerRight : Alignment.centerLeft,
      child: Container(
        width: _listTileWidth,
        decoration: BoxDecoration(
          borderRadius: _borderRadius,
          color: fromSelfUser ? Color(0xFFE2FFC7) : Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey[600].withOpacity(0.35),
              blurRadius: 0.5,
              offset: Offset(0, 0.5),
            ),
          ],
        ),
        padding: EdgeInsets.symmetric(
          horizontal: _horizontalPadding,
          vertical: _verticalPadding,
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Flexible(
              child: Align(
                alignment: Alignment.topLeft,
                child: Text(
                  text,
                  style: _messageTextStyle,
                ),
              ),
            ),
            SizedBox(width: 4),
            Align(
              alignment: Alignment.bottomRight,
              child: Text(
                dateTime,
                style: _timeTextStyle,
              ),
            ),
            if (fromSelfUser) SizedBox(width: 5),
            if (fromSelfUser)
              Align(
                alignment: Alignment.bottomRight,
                child: ReceiptCheckIcon(
                  status,
                  size: 15,
                ),
              ),
          ],
        ),
      ),
    );
  }

  // Here it is!
  Size _layoutTextSize(String text, TextStyle style) {
    final TextPainter textPainter = TextPainter(
        text: TextSpan(text: text, style: style),
        maxLines: 1,
        textDirection: TextDirection.ltr)
      ..layout(minWidth: 0, maxWidth: double.infinity);
    return textPainter.size;
  }
}
