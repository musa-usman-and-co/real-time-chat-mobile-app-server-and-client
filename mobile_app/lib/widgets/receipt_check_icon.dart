import 'package:flutter/material.dart';

/// Created by Musa Usman on 22.01.2021
/// Copyright © 2021 Musa Usman & Co. All rights reserved.
///
/// Email: hello@musausman.com
/// Website: musausman.com
/// WhatsApp: +1 307-459-1575

class ReceiptCheckIcon extends StatelessWidget {
  final int status;
  final double size;

  const ReceiptCheckIcon(this.status, {this.size});

  Color get _color => status == 4 ? Color(0xFF34B7F1) : Colors.grey[400];

  double get _size => this.size ?? (status == 1 ? 16 : 18);

  IconData get _icon => status >= 3
      ? Icons.done_all
      : status == 2
          ? Icons.done
          : Icons.timer;

  @override
  Widget build(BuildContext context) => Icon(
        _icon,
        color: _color,
        size: _size,
      );
}
