import 'package:chat_app/pages/chat_page.dart';
import 'package:chat_app/utils/globals.dart';
import 'package:chat_app/utils/models.dart';
import 'package:chat_app/widgets/receipt_check_icon.dart';
import 'package:flutter/material.dart';

/// Created by Musa Usman on 21.01.2021
/// Copyright © 2021 Musa Usman & Co. All rights reserved.
///
/// Email: hello@musausman.com
/// Website: musausman.com
/// WhatsApp: +1 307-459-1575

class ConversationListItem extends StatelessWidget {
  final Conversation conversation;

  const ConversationListItem(this.conversation);

  String get _lastMessageTimeString {
    DateTime _currentDateTime = conversation.lastMessage.dateTime;

    DateTime _dateTime = conversation.lastMessage.dateTime;

    if (_dateTime.day == _currentDateTime.day &&
        _dateTime.month == _currentDateTime.month &&
        _dateTime.year == _currentDateTime.year)
      return conversation.lastMessage.dateTimeString.toUpperCase();

    return conversation.lastMessage.dateTime.toString();
  }

  @override
  Widget build(BuildContext context) => InkWell(
        onTap: () => _onTap(context),
        splashColor: Colors.blueGrey[100].withOpacity(.25),
        highlightColor: Colors.blueGrey[100].withOpacity(.18),
        child: Container(
          padding: EdgeInsets.symmetric(
            vertical: 15,
            horizontal: 15,
          ),
          child: Row(
            children: [
              Container(
                height: 45,
                width: 45,
                decoration: BoxDecoration(
                  color: Colors.grey[300],
                  shape: BoxShape.circle,
                ),
                child: Center(
                  child: Icon(
                    Icons.person,
                    color: Colors.white,
                  ),
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: Text(
                            conversation.otherUserName,
                            style: Theme.of(context).textTheme.subtitle2,
                          ),
                        ),
                        Text(
                          _lastMessageTimeString,
                          maxLines: 2,
                          style: Theme.of(context).textTheme.caption.copyWith(
                                fontWeight: FontWeight.w500,
                                color: conversation.unreadMessageCount > 0
                                    ? Theme.of(context).primaryColor
                                    : null,
                                fontSize: 13,
                              ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        if (conversation.lastMessage.senderID == selfUserID)
                          ReceiptCheckIcon(conversation.lastMessage.status),
                        if (conversation.lastMessage.senderID == selfUserID)
                          SizedBox(
                            width: 5,
                          ),
                        Expanded(
                          child: Text(
                            conversation.lastMessage.text,
                            maxLines: 2,
                            style: Theme.of(context).textTheme.caption.copyWith(
                                  fontSize: 13,
                                ),
                          ),
                        ),
                        if (conversation.unreadMessageCount > 0)
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 5),
                            height: 19,
                            constraints: BoxConstraints(
                              minWidth: 19,
                            ),
                            decoration: BoxDecoration(
                              color: Colors.blue,
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: Center(
                              child: Text(
                                conversation.unreadMessageCount.toString(),
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );

  void _onTap(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (BuildContext context) => ChatPage(
          otherUserID: conversation.otherUserID,
          otherUserName: conversation.otherUserName,
        ),
      ),
    );
  }
}
