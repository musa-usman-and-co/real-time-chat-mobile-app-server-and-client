import 'package:flutter/material.dart';

/// Created by Musa Usman on 21.01.2021
/// Copyright © 2021 Musa Usman & Co. All rights reserved.
///
/// Email: hello@musausman.com
/// Website: musausman.com
/// WhatsApp: +1 307-459-1575

class NewMessageDialog extends StatelessWidget {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _idController = TextEditingController();

  @override
  Widget build(BuildContext context) => Dialog(
        // backgroundColor: Theme.of(context).accentColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: 25,
            vertical: 30,
          ),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  "New Chat",
                  style: Theme.of(context)
                      .textTheme
                      .headline5
                      .copyWith(fontWeight: FontWeight.bold),
                ),
                TextFormField(
                  controller: _idController,
                  keyboardType: TextInputType.number,
                  validator: (_) => _.isEmpty
                      ? "You can leave this field empty."
                      : RegExp(r"[A-z]").hasMatch(_)
                          ? "Invalid ID"
                          : null,
                  decoration: InputDecoration(
                    hintText: "1234",
                    labelText: "Enter Contact ID",
                    icon: Container(
                      margin: EdgeInsets.only(
                        top: 13,
                      ),
                      child: Icon(
                        Icons.person_outline_rounded,
                        size: 30,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: FlatButton(
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(
                          "START CHAT",
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Icon(
                          Icons.arrow_forward,
                          color: Theme.of(context).accentColor,
                        ),
                      ],
                    ),
                    onPressed: () => _startChat(context),
                  ),
                ),
              ],
            ),
          ),
        ),
      );

  void _startChat(BuildContext context) {
    if (_formKey.currentState.validate()) {
      int _otherID = int.parse(_idController.text);

      Navigator.pop(context, _otherID);
    }
  }
}
